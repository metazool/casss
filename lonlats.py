from urllib2 import urlopen
import _mysql

"""
First, do this to the database to make a geometry type column (called 'll')
mysql> alter table CASSS_Vol_1 add ll POINT;

"""

db = _mysql.connect("localhost","root","XXXX","casss")

convert = "http://www.nearby.org.uk/api/convert.php?key=49c2dcfb158c27&in=gr-osgb36&want=ll-wgs84&output=text&p="

gridrefs = {}

db.query("select id, OS_100KM_SQUARE , OS_GRID_REFERENCE from CASSS_Vol_1")
r=db.store_result()
rows = r.fetch_row(maxrows=0)

for row in rows:
    ll = None
    osgrid = row[1]+row[2]

    """Many stones share a location so let's not hit converter unnecessarily"""
    if gridrefs.has_key(osgrid):
        ll = gridrefs[osgrid]

    else:
        try: 
            c = urlopen(convert+osgrid).read().strip().split(',')
            lat = c[2]
            lon = c[3] 
            ll = (lon, lat)
            gridrefs[osgrid] = ll
        except:
            continue

    geom = "GeomFromText('POINT("+' '.join(ll)+")')"

    query = "UPDATE CASSS_Vol_1 SET ll = "+geom+" WHERE id = "+row[0] 
  
    print query 
 
    db.query(query) 

"""I used nearby.org.uk to convert OSGB references to WGS84 lon, lat 
 ( WGS84 is the default coordinate reference system used in GPS units, by KML, GeoJSON, etc) """


